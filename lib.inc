section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60 
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
.loop:
    cmp byte [rdi + rax], 0
    je .end
    inc rax
    jmp .loop
.end:
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rax
    push rdi
    call string_length
    mov rsi, [rsp]
    mov rdx, rax
    mov rax, 1
    mov rdi, 1
    push rcx
    syscall
    pop rcx
    pop rdi
    pop rax

    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rsi, rsp
    mov rax, 1
    mov rdi, 1
    mov rdx, 1
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA
    call print_char
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov r8, 10
    mov rax, rdi
    mov rdi, rsp
    dec rdi
    push 0
    sub rsp, 16
.loop:
    xor rdx, rdx
    div r8
    add rdx, '0'
    dec rdi
    mov [rdi], dl
    test rax, rax
    jnz .loop
    call print_string
    add rsp, 24
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    cmp rdi, 0
    jl .neg
    jmp .end
.neg:
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi
.end:
    call print_uint
    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rcx, rcx
    xor r9, r9
    xor r10, r10
.loop:
    mov r9b, byte [rdi + rcx]
    mov r10b, byte [rsi + rcx]
    cmp r9b, r10b
    jne .not_equals
    cmp r9b, 0
    je .equals
    inc rcx
    jmp .loop
.equals:                 
    mov rax, 1
    ret
.not_equals:             
    mov rax, 0
    ret


    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax
    xor rdi, rdi
    push 0
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rax
    ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
.space_loop:
    push rdi
    push rsi
    call read_char
    pop rsi
    pop rdi
    cmp rax, 0x20
    je .space_loop
    cmp rax, 0x9
    je .space_loop
    cmp rax, 0xA
    je .space_loop
    xor rdx, rdx
.loop:
    test rax, rax
    jz .end
    mov [rdi + rdx], rax
    inc rdx
    cmp rdx, rsi
    je .error
    push rdi
    push rsi
    push rdx
    call read_char
    pop rdx
    pop rsi
    pop rdi
    cmp rax, 0x20
    je .add_null
    cmp rax, 0x9
    je .add_null
    cmp rax, 0xA
    je .add_null
    jmp .loop
.error:
    xor rax, rax
    ret
.add_null:
    mov byte[rdi + rdx], 0
.end:
    mov rax, rdi
    ret

 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor r9, r9
    xor rdx, rdx
    xor rax, rax
.loop:
    cmp byte [rdi + rdx], '0'
    jl .end
    cmp byte [rdi + rdx], '9'
    jg .end
    mov r9b, byte [rdi + rdx]
    sub r9b, '0'
    imul rax, 10
    add rax, r9
    inc rdx
    jmp .loop
.end:
    ret


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp byte [rdi], '-'
    jne .positive
    inc rdi
    call parse_uint
    inc rdx
    neg rax
    ret
.positive:
    jmp parse_uint


; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor r8, r8
    push rdi
    push rsi
    push rdx
    call string_length
    pop rdx
    pop rsi
    pop rdi
    cmp rax, rdx
    ja .error
.loop:
    mov r8b, byte [rdi]
    mov byte [rsi], r8b
    inc rdi
    inc rsi
    test r8b, r8b
    jnz .loop
    ret
.error:
    xor rax, rax
    ret
